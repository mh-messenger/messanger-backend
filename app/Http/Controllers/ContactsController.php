<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserContactResource;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function __construct() {
        $this->middleware(['auth:sanctum']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        $user = auth('sanctum')->user();

        return UserContactResource::collection(
            $user->contacts()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id): \Illuminate\Http\Response
    {
        $user = auth('sanctum')->user();

        $contact = $user->contacts()->find($id);

        if(!$contact->exists()) {
            return response([
                'message' => 'contact.not.found'
            ], 400);
        }

        $user->contacts()->attach($id, [
            'first_name' => $contact->first_name,
            'last_name' => $contact->last_name
        ]);

        return response([
            'message' => 'contact.added'
        ],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return UserContactResource|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $user = auth('sanctum')->user();

        $contact = $user->contacts()->find($id);

        if(!$contact->exists()) {
            return response([
                'message' => 'contact.not.found'
            ], 400);
        }

        return new UserContactResource($contact);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id): \Illuminate\Http\Response
    {
        $user = auth('sanctum')->user();

        $contact = $user->contacts()->find($id);

        if(!$contact->exists()) {
            return response([
                'message' => 'contact.not.found'
            ], 400);
        }

        $validated = $request->validate([
            'first_name' => 'string|max:40',
            'last_name' => 'string|max:40'
        ]);

        $contact->update($validated);

        return response([
            'message' => 'contact.updated'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id): \Illuminate\Http\Response
    {
        $user = auth('sanctum')->user();

        $contact = $user->contacts()->find($id);

        if(!$contact->exists()) {
            return response([
                'message' => 'contact.not.found'
            ], 400);
        }

        $user->detach([$contact->id]);
    }
}
