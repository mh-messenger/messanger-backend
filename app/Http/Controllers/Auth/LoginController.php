<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /**
     * @param  LoginRequest  $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function login(LoginRequest $request): \Illuminate\Http\JsonResponse
    {
        $user = $this->user($request->validated());

        $this->checkUserPassword($user, $request);

        $token = $this->createAccessToken($user);

        return response()->json([
            'message' => 'Успешная авторизация!',
            'access_token' => $token
        ], 200);
    }

    /**
     * @param  array  $data
     * @return mixed
     */
    private function user(array $data)
    {
        return User::where('email', $data['email'])->first();
    }

    /**
     * @param  User  $user
     * @return string
     */
    private function createAccessToken(User $user): string
    {
        return $user->createToken(Str::random(),[])->plainTextToken;
    }
    /**
     * @param $user
     * @param  LoginRequest  $request
     * @throws ValidationException
     */
    private function checkUserPassword($user, LoginRequest $request): void
    {
        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'The provided credentials are incorrect.',
            ]);
        }
    }
}
